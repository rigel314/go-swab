package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
)

var swabsize = flag.Int("s", 4, "swab size")
var bufsize = flag.Int("buf", 1<<12, "buffer size")

func main() {
	flag.Parse()

	if *swabsize > *bufsize {
		panic(fmt.Sprintf("bufsize(%d) smaller than swabsize(%d)", *bufsize, *swabsize))
	}

	buf := make([]byte, *bufsize)
	for _, fpath := range flag.Args() {
		f := mustOpen(fpath)
		defer f.Close()
		size := mustSeek(f, 0, os.SEEK_END)
		if size%int64(*swabsize) != 0 {
			panic(fmt.Sprintf("filesize(%d) not multiple of swabsize(%d)", size, *swabsize))
		}
		mustSeek(f, 0, os.SEEK_SET)

		var pos int64
		for {
			n, err := f.Read(buf)
			if err != nil {
				if errors.Is(err, io.EOF) {
					break
				}
				panic(err)
			}
			for i := 0; i < n / *swabsize; i++ {
				for j := 0; j < *swabsize/2; j++ {
					buf[i**swabsize+j], buf[i**swabsize+(*swabsize-j-1)] = buf[i**swabsize+(*swabsize-j-1)], buf[i**swabsize+j]
				}
			}
			wn, err := f.WriteAt(buf[:n], pos)
			if err != nil {
				panic(err)
			}
			if wn != n {
				panic("incomplete write")
			}
			pos += int64(n)
		}
	}
}

func mustOpen(path string) *os.File {
	f, err := os.OpenFile(path, os.O_RDWR, 0644)
	if err != nil {
		panic(err)
	}
	return f
}

func mustSeek(f *os.File, off int64, whence int) int64 {
	r, err := f.Seek(off, whence)
	if err != nil {
		panic(err)
	}

	return r
}
